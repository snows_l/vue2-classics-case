/*
 * @Description: ------------ fileDescription -----------
 * @Author: snows_l snows_l@163.com
 * @Date: 2023-10-26 09:19:33
 * @LastEditors: snows_l snows_l@163.com
 * @LastEditTime: 2023-11-10 11:09:16
 * @FilePath: /vue2-classics-case/src/router/index.js
 */
import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home/index.vue';

// 解决重复点击相同路由是报错
const routerPush = VueRouter.prototype.push;
VueRouter.prototype.push = function (location) {
  return routerPush.call(this, location).catch(err => {});
};

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Index',
    component: Home
  },
  {
    path: '/form-multiple-validate',
    name: 'formMultipleValidate',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/formMultipleValidate/index.vue')
  },
  {
    path: '/form-in-table',
    name: 'FormInTable',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/FormInTable/index.vue')
  },
  {
    path: '/base64',
    name: 'UploadFile2Base64',
    component: () => import(/* webpackChunkName: "about" */ '../views/UploadFile2Base64/index.vue')
  },
  {
    path: '/full-screen',
    name: 'FullScreen',
    component: () => import(/* webpackChunkName: "about" */ '../views/FullScreen/index.vue')
  },
  {
    path: '/draw-line',
    name: 'DrawLine',
    component: () => import(/* webpackChunkName: "about" */ '../views/DrawLine/index.vue')
  },

  {
    path: '/rsa',
    name: 'RSA',
    component: () => import(/* webpackChunkName: "about" */ '../views/Rsa/index.vue')
  }
];

const router = new VueRouter({
  routes
});

export default router;
