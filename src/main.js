/*
 * @Description: ------------ fileDescription -----------
 * @Author: snows_l snows_l@163.com
 * @Date: 2023-10-26 09:19:33
 * @LastEditors: snows_l snows_l@163.com
 * @LastEditTime: 2023-10-26 09:37:43
 * @FilePath: /vue2-classics-case/src/main.js
 */
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import Vue from 'vue';
import App from './App.vue';
import './assets/reset.css';
import router from './router';
import store from './store';

Vue.use(ElementUI);
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
