/*
 * @Description: ------------ fileDescription -----------
 * @Author: snows_l snows_l@163.com
 * @Date: 2023-11-09 16:25:29
 * @LastEditors: snows_l snows_l@163.com
 * @LastEditTime: 2023-11-10 14:11:55
 * @FilePath: /vue2-classics-case/src/utils/drawLine.js
 */

/**
 * @description: 绘画出两个 div 之间的连线
 * @param {*} containerId 插入到容器的 id
 * @param {*} startElementId 起点元素的 id
 * @param {*} endElementId 终点元素的 id
 * @param {*} color 线条的颜色
 * @param {*} lineWidth 线条的宽度  默认 2px
 * @param {*} lineClass 线条的类名 默认 l-line  （用于后续删除线条）
 */
export const drawLine = (containerId, startElementId, endElementId, color, lineWidth = '2px', lineClass = 'l-line') => {
  // 获取起点终点的容器
  let startElement = document.getElementById(startElementId);
  let endElement = document.getElementById(endElementId);
  let container = document.getElementById(containerId);

  let isHorizontal = false;
  if (!startElement || !endElement || !container) return;

  /**
   *  起点坐标
   */
  //  相对于视口
  // let x1 = startElement.getBoundingClientRect().left + startElement.clientWidth / 2;
  // let y1 = startElement.getBoundingClientRect().top + startElement.clientHeight;
  // 相对于夫级元素
  let x1 = startElement.offsetLeft + (isHorizontal ? startElement.clientWidth : startElement.clientWidth / 2);
  let y1 = startElement.offsetTop + (isHorizontal ? startElement.clientHeight / 2 : startElement.clientHeight);

  /**
   *  终点坐标
   */
  //  相对于视口
  // let x2 = endElement.getBoundingClientRect().left + endElement.clientWidth / 2;
  // let y2 = endElement.getBoundingClientRect().top;
  // 相对于夫级元素
  let x2 = endElement.offsetLeft + (isHorizontal ? 0 : endElement.clientWidth / 2);
  let y2 = endElement.offsetTop + (isHorizontal ? endElement.clientHeight / 2 : 0);

  // 计算连接线长度
  let length = Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));

  // 计算连接线旋转弧度值
  let rad = Math.atan2(y2 - y1, x2 - x1);

  // 连接线未旋转前，起点坐标计算
  let top = (y1 + y2) / 2;
  let left = (x1 + x2) / 2 - length / 2;

  // 创建连接线 dom 节点，并设置样式
  let line = document.createElement('div');
  let style =
    'position: absolute; background-color: ' +
    color +
    '; height: ' +
    lineWidth +
    '; top:' +
    top +
    'px; left:' +
    left +
    'px; width: ' +
    length +
    'px; transform: rotate(' +
    rad +
    'rad);';
  line.setAttribute('class', lineClass);
  line.setAttribute('style', style);
  container.appendChild(line);
};

/**
 * @description: 清空所有线条
 * @param {*} lineClass 需要清空的 线条的 class
 */
export const cleanLine = lineClass => {
  let lineElements = document.querySelectorAll('.' + lineClass);
  if (!lineElements.length) return;
  lineElements.forEach(item => {
    item.remove();
  });
};

/**
 * @description: 移除指定的线条
 * @param {*} lineClass 线条的 class
 */
export const removeLine = lineClass => {
  let lineElements = document.querySelectorAll('.' + lineClass);
  if (!lineElements.length) return;
  lineElements[0].remove();
};

export default { drawLine, removeLine, cleanLine };
