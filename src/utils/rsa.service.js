/*
 * @Description: ------------ fileDescription -----------
 * @Author: snows_l snows_l@163.com
 * @Date: 2023-11-10 11:13:32
 * @LastEditors: snows_l snows_l@163.com
 * @LastEditTime: 2023-11-10 11:13:35
 * @FilePath: /vue2-classics-case/src/utils/rsa.service.js
 */
import JSEncrypt from 'jsencrypt';
import Encrypt from 'encryptlong';

//rsa加密
export function rsaEncrypt(obj) {
  if (obj == null) {
    return;
  }
  const jse = new JSEncrypt();
  jse.setPublicKey(`-----BEGIN PUBLIC KEY-----
        MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC+BRO2pwfAkFUpTSUynKjHbc/ZpojfaXUU6kAeN+XTfCQlUvRhw4VC5ZtH0oD2sXgEuAYj1HsUkZPXNzAknZ6tE9q24EwlrFYmct/Hyn3JcvpA1dfanUFYHPWLiHeeNZYgA5MLXZiu0fS5X3et8e83/JkAmVOSoVrsUcbp85oIcwIDAQAB
        -----END PUBLIC KEY-----`);
  const jsonObj = JSON.stringify(obj);
  const encrypted = jse.encrypt(jsonObj);
  return encrypted;
}

// 解密 用于解密过长的地方
export function decrypt(data) {
  let privateKey = `-----BEGIN PRIVATE KEY-----
MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAL4FE7anB8CQVSlNJTKcqMdtz9mmiN9pdRTqQB435dN8JCVS9GHDhULlm0fSgPaxeAS4BiPUexSRk9c3MCSdnq0T2rbgTCWsViZy38fKfcly+kDV19qdQVgc9YuId541liADkwtdmK7R9Llfd63x7zf8mQCZU5KhWuxRxunzmghzAgMBAAECgYB4ql6vYXghx0XdGVkiv6rKIaiYzREj1iL8ahjXV6XBW5bUgM4heviX15h4zmsIvP+692UmwZp7IEriovAWgGUu6Utgpezfp2k5IbTJVuhkT6LUWqExkqPxfsvBkVFREJNiDt1F45nAKXS49IzVynUpufNvTBpbJ4RHmuUuRFfvuQJBAN0rCFqP2SONu6BM1M8KlR/jqV+xY5vEBOqre5Uil/th2z2DRB/MsaLCgpb1QtoUXaJjIRLE4sEwwGb3Zo+kk7UCQQDb8jiWUo3SovPLcfP/w4Vf1TvaYgIuRKCYF6GmiJBhe9wv7I0WZMPT/CKWNIQI+A1byMDNdB+9CCRB+4Gu7hSHAkEAk9zcReecDTgJs58KO61gi6RiLVbkOFRx0Q56nfc10tvWfqaO2g+4xw7xzckFO9WX0CQkDW9SBkT4rR5EFE68bQJAa/RUr/uvHq7aqIWy9FtcUHBR41ttpJYMmPooXpvy+dYYefFKQq6Mq7S6qSz3jYtqyx0a/Py9Q0QGv9gZi+nRawJAf2T6ryfkCAtYrtO2CTmr7MF2gmt7GI7O547m9exSHfjNBouzVzZoHTu8ptnMOfvY5OTZB3M5+Yjwc1fu1vhJRA==
     -----END PRIVATE KEY-----`;
  const PRIVATE_KEY = privateKey;
  var encryptor = new Encrypt();
  encryptor.setPrivateKey(PRIVATE_KEY); // 如果是对象/数组的话，需要先JSON.stringify转换成字符串
  var result = encryptor.decryptLong(data);
  return result;
}
