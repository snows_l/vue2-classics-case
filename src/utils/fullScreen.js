/*
 * @Description: ------------ 全屏逻辑 -----------
 * @Author: snows_l snows_l@163.com
 * @Date: 2023-11-09 09:50:10
 * @LastEditors: snows_l snows_l@163.com
 * @LastEditTime: 2023-11-09 14:41:34
 * @FilePath: /vue2-classics-case/src/utils/fullScreen.js
 */
class FullScreen {
  constructor(ele) {
    this.ele = null;
    this.isEnabled = null;
    this.isFullscreen = false;
    this.setElement(ele);
    this.init();
  }

  /**
   * @description: 初始化
   * @return {*}
   */
  init() {
    if (this.isEnabledFn()) {
      this.isEnabled = this.isEnabledFn();
    } else {
      throw new Error('你的浏览器不支持全屏模式！请换一个浏览器试试。');
    }
  }

  /**
   * @description: 是否支持全屏
   * @return {*}
   */
  isEnabledFn() {
    return document.fullscreenEnabled || document.mozFullScreenEnabled || document.webkitFullscreenEnabled || document.msFullscreenEnabled || false;
  }

  /**
   * @description: 是否是全屏
   * @description: 第一次拿到的时候是 NULL
   * @description: 注意：第二次之后 全屏就是 null ,不是全屏就是 this.ele
   * @return {*}
   */
  isFullscreenFn() {
    return !(document.fullscreenElement || document.msFullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement || false);
  }

  /**
   * @description: 默认设置 body 为全屏的容器  拿到实例纸都可以调用这个方法重新设置需要 全屏的容器
   * @return {*}
   */
  setElement(ele) {
    if (!ele) {
      this.ele = document.getElementsByTagName('body')[0];
    } else {
      if (ele instanceof Element) {
        this.ele = ele;
      } else if (Object.prototype.toString.call(ele).slice(8, -1) === 'String') {
        if (ele == 'html' || ele == 'body') this.ele = document.getElementsByTagName(ele)[0];
        else this.ele = document.getElementById(ele);
      } else {
        throw new Error(`${ele} is not found, ${ele} should be Id of String type or Element or empty`);
      }
    }
  }

  /**
   * @description: 全屏 切换
   * @param {*} callback 回调函数
   * @return {*}
   */
  onToggle(callback) {
    if (this.isEnabled) {
      if (this.isFullscreen) {
        this.onClose(callback);
      } else {
        this.onOpen(callback);
      }
    }
  }

  /**
   * @description: 进入 全屏
   * @param {*} callback 回调函数
   * @return {*}
   */
  onOpen(callback) {
    if (this.ele.requestFullscreen) {
      this.ele.requestFullscreen();
    } else if (this.ele.webkitRequestFullScreen) {
      this.ele.webkitRequestFullScreen();
    } else if (this.ele.mozRequestFullScreen) {
      this.ele.mozRequestFullScreen();
    } else if (this.ele.msRequestFullscreen) {
      // IE11
      this.ele.msRequestFullscreen();
    }

    // 获取 是否是全屏 是就调用回调
    this.isFullscreen = this.isFullscreenFn();
    if (this.isFullscreen && callback) callback();
  }

  /**
   * @description: 退出 全屏
   * @return {*}
   */
  onClose(callback) {
    if (document.exitFullscreen) {
      document.exitFullscreen();
    } else if (document.webkitCancelFullScreen) {
      document.webkitCancelFullScreen();
    } else if (document.mozCancelFullScreen) {
      document.mozCancelFullScreen();
    } else if (document.msExitFullscreen) {
      document.msExitFullscreen();
    }

    // 获取 是否是全屏 不是就调用回调
    this.isFullscreen = this.isFullscreenFn();
    if (!this.isFullscreen && callback) callback();
  }
}

export default new FullScreen();
