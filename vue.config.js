/*
 * @Description: ------------ fileDescription -----------
 * @Author: snows_l snows_l@163.com
 * @Date: 2023-10-26 09:19:33
 * @LastEditors: snows_l snows_l@163.com
 * @LastEditTime: 2023-10-26 09:23:20
 * @FilePath: /vue2-classics-case/vue.config.js
 */
const { defineConfig } = require('@vue/cli-service');
module.exports = defineConfig({
  transpileDependencies: true,
  lintOnSave: false
});
